# Use the official Node.js image as a base image
FROM node:14

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY server/ ./server/
COPY config/config.js ./config/

# Expose the port the app runs on
EXPOSE 3000

# Command to run the application
CMD ["node", "server/server.js"]
