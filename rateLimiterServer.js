const express = require('express');
const rateLimit = require('express-rate-limit');
const RATE_LIMIT_WINDOW_MS = 60000;
const RATE_LIMIT_MAX_REQUESTS = 100;

const app = express();

// Middleware giới hạn tốc độ truy cập
const limiter = rateLimit({
    windowMs: RATE_LIMIT_WINDOW_MS,
    max: RATE_LIMIT_MAX_REQUESTS,
    message: 'Too many requests, please try again later.',
});

app.use(express.json());

// API endpoint giới hạn tốc độ truy cập
app.get('/api/rate-limited', limiter, (req, res) => {
    res.json({ message: 'Rate limited API call' });
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log(`Rate Limiter Server is running on port ${PORT}`);
});
