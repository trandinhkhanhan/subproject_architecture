const express = require('express');
const axios = require('axios');
const rateLimit = require('express-rate-limit');
const CircuitBreaker = require('circuit-breaker-js');
const { RATE_LIMIT_WINDOW_MS, RATE_LIMIT_MAX_REQUESTS } = require('../config/config');

const app = express();

app.get('/', (req, res) => {
    res.send('Server is running successfully!');
});

const PORT = process.env.PORT || 3000;

// Middleware giới hạn tốc độ truy cập
const limiter = rateLimit({
    windowMs: RATE_LIMIT_WINDOW_MS,
    max: RATE_LIMIT_MAX_REQUESTS,
    message: 'Too many requests, please try again later.',
});

// Circuit Breaker cho service API call
const circuitBreaker = new CircuitBreaker(axios.get, {
    timeoutDuration: 10000, // 10 giây
    volumeThreshold: 5,
    errorThreshold: 50,
    enabled: true,
});

app.use(express.json());

// API endpoint bảo vệ bởi giới hạn tốc độ truy cập
app.get('/api/protected', limiter, async(req, res) => {
    try {
        const response = await axios.get('http://localhost:3000/');
        res.json(response.data);
    } catch (error) {
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// API endpoint sử dụng Circuit Breaker
app.get('/api/service', async(req, res) => {
    try {
        const response = await circuitBreaker.fire('http://localhost:3000/');
        res.json(response.data);
    } catch (error) {
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// Endpoint cho các service mới
app.post('/api/publish', async(req, res) => {
    // Logic xử lý phát hành phím
    res.json({ message: 'Key published successfully' });
});

app.post('/api/keyboard', async(req, res) => {
    // Logic xử lý thông tin về các loại phím
    res.json({ message: 'Keyboard insert successfully' });
});

app.post('/api/user/:userId', async(req, res) => {
    // Logic xử lý thông tin người dùng
    res.json({ message: 'User insert successfully' });
});

app.post('/api/maker/:makerId', async(req, res) => {
    // Logic xử lý thông tin chủ phím
    res.json({ message: 'Maker insert successfully' });
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});