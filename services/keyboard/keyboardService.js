const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let keyboards = [];

// Lấy danh sách tất cả các bàn phím
app.get('/api/keyboards', (req, res) => {
    res.json({ keyboards });
});

// Lấy thông tin chi tiết của một bàn phím cụ thể
app.get('/api/keyboards/:keyboardId', (req, res) => {
    const { keyboardId } = req.params;
    const keyboard = keyboards.find(k => k.id === parseInt(keyboardId));
    if (!keyboard) {
        return res.status(404).json({ message: 'Keyboard not found' });
    }
    res.json({ keyboard });
});

// Thêm một bàn phím mới
app.post('/api/keyboards', (req, res) => {
    const { brand, model } = req.body;
    const newKeyboard = {
        id: keyboards.length + 1,
        brand,
        model
    };
    keyboards.push(newKeyboard);
    res.status(201).json(newKeyboard);
});

// Cập nhật một bàn phím cụ thể
app.put('/api/keyboards/:keyboardId', (req, res) => {
    const { keyboardId } = req.params;
    const { brand, model } = req.body;
    const keyboardIndex = keyboards.findIndex(k => k.id === parseInt(keyboardId));
    if (keyboardIndex === -1) {
        return res.status(404).json({ message: 'Keyboard not found' });
    }
    keyboards[keyboardIndex] = { id: parseInt(keyboardId), brand, model };
    res.json(keyboards[keyboardIndex]);
});

// Xóa một bàn phím cụ thể
app.delete('/api/keyboards/:keyboardId', (req, res) => {
    const { keyboardId } = req.params;
    const keyboardIndex = keyboards.findIndex(k => k.id === parseInt(keyboardId));
    if (keyboardIndex === -1) {
        return res.status(404).json({ message: 'Keyboard not found' });
    }
    keyboards.splice(keyboardIndex, 1);
    res.status(204).send();
});

const PORT = process.env.PORT || 4005;
app.listen(PORT, () => {
    console.log(`Keyboard Service is running on port ${PORT}`);
});