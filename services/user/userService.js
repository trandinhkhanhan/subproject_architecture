const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let users = [];

// Lấy danh sách tất cả các người dùng
app.get('/api/users', (req, res) => {
    res.json({ users });
});

// Thêm một người dùng mới vào danh sách
app.post('/api/users', (req, res) => {
    const { user } = req.body;
    users.push(user);
    res.status(201).json({ message: 'User stored successfully' });
});

// Cập nhật thông tin của một người dùng cụ thể
app.put('/api/users/:userId', (req, res) => {
    const { userId } = req.params;
    const { user } = req.body;
    const userIndex = users.findIndex(u => u.id === parseInt(userId));
    if (userIndex === -1) {
        return res.status(404).json({ message: 'User not found' });
    }
    users[userIndex] = { id: parseInt(userId), ...user };
    res.json({ message: 'User updated successfully' });
});

// Xóa một người dùng cụ thể khỏi danh sách
app.delete('/api/users/:userId', (req, res) => {
    const { userId } = req.params;
    const userIndex = users.findIndex(u => u.id === parseInt(userId));
    if (userIndex === -1) {
        return res.status(404).json({ message: 'User not found' });
    }
    users.splice(userIndex, 1);
    res.status(204).json({ message: 'User deleted successfully' });
});

const PORT = process.env.PORT || 4003;
app.listen(PORT, () => {
    console.log(`User Service is running on port ${PORT}`);
});