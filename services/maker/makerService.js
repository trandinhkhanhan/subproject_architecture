const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let makers = [];

// Lấy danh sách tất cả các maker
app.get('/api/makers', (req, res) => {
    res.json({ makers });
});

// Lấy thông tin chi tiết của một maker cụ thể
app.get('/api/makers/:makerId', (req, res) => {
    const { makerId } = req.params;
    const maker = makers.find(m => m.id === parseInt(makerId));
    if (!maker) {
        return res.status(404).json({ message: 'Maker not found' });
    }
    res.json({ maker });
});

// Thêm một maker mới
app.post('/api/makers', (req, res) => {
    const { name } = req.body;
    const newMaker = {
        id: makers.length + 1,
        name
    };
    makers.push(newMaker);
    res.status(201).json(newMaker);
});

// Cập nhật thông tin của một maker cụ thể
app.put('/api/makers/:makerId', (req, res) => {
    const { makerId } = req.params;
    const { name } = req.body;
    const makerIndex = makers.findIndex(m => m.id === parseInt(makerId));
    if (makerIndex === -1) {
        return res.status(404).json({ message: 'Maker not found' });
    }
    makers[makerIndex] = { id: parseInt(makerId), name };
    res.json(makers[makerIndex]);
});

// Xóa một maker cụ thể
app.delete('/api/makers/:makerId', (req, res) => {
    const { makerId } = req.params;
    const makerIndex = makers.findIndex(m => m.id === parseInt(makerId));
    if (makerIndex === -1) {
        return res.status(404).json({ message: 'Maker not found' });
    }
    makers.splice(makerIndex, 1);
    res.status(204).send();
});

const PORT = process.env.PORT || 4004;
app.listen(PORT, () => {
    console.log(`Maker Service is running on port ${PORT}`);
});