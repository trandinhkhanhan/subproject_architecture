const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let publishedKeys = [];

// Lấy danh sách tất cả các phím đã được phát hành
app.get('/api/published-keys', (req, res) => {
    res.json({ publishedKeys });
});

// Thêm một phím mới vào danh sách phím đã phát hành
app.post('/api/publish', (req, res) => {
    const { key } = req.body;
    publishedKeys.push(key);
    res.status(201).json({ message: 'Key published successfully' });
});

// Cập nhật thông tin của một phím đã phát hành
app.put('/api/published-keys/:keyId', (req, res) => {
    const { keyId } = req.params;
    const { key } = req.body;
    const keyIndex = publishedKeys.findIndex(k => k.id === parseInt(keyId));
    if (keyIndex === -1) {
        return res.status(404).json({ message: 'Key not found' });
    }
    publishedKeys[keyIndex] = { id: parseInt(keyId), key };
    res.json({ message: 'Key updated successfully' });
});

// Xóa một phím khỏi danh sách phím đã phát hành
app.delete('/api/published-keys/:keyId', (req, res) => {
    const { keyId } = req.params;
    const keyIndex = publishedKeys.findIndex(k => k.id === parseInt(keyId));
    if (keyIndex === -1) {
        return res.status(404).json({ message: 'Key not found' });
    }
    publishedKeys.splice(keyIndex, 1);
    res.status(204).json({ message: 'Key deleted successfully' });
});

const PORT = process.env.PORT || 4001;
app.listen(PORT, () => {
    console.log(`Publish Service is running on port ${PORT}`);
});