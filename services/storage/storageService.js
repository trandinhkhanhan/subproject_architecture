const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let keyboards = [];

// Lấy danh sách tất cả các bàn phím
app.get('/api/keyboards', (req, res) => {
    res.json({ keyboards });
});

// Thêm một bàn phím mới vào danh sách
app.post('/api/keyboards', (req, res) => {
    const { keyboard } = req.body;
    keyboards.push(keyboard);
    res.status(201).json({ message: 'Keyboard stored successfully' });
});

// Cập nhật thông tin của một bàn phím cụ thể
app.put('/api/keyboards/:keyboardId', (req, res) => {
    const { keyboardId } = req.params;
    const { keyboard } = req.body;
    const keyboardIndex = keyboards.findIndex(k => k.id === parseInt(keyboardId));
    if (keyboardIndex === -1) {
        return res.status(404).json({ message: 'Keyboard not found' });
    }
    keyboards[keyboardIndex] = { id: parseInt(keyboardId), ...keyboard };
    res.json({ message: 'Keyboard updated successfully' });
});

// Xóa một bàn phím cụ thể khỏi danh sách
app.delete('/api/keyboards/:keyboardId', (req, res) => {
    const { keyboardId } = req.params;
    const keyboardIndex = keyboards.findIndex(k => k.id === parseInt(keyboardId));
    if (keyboardIndex === -1) {
        return res.status(404).json({ message: 'Keyboard not found' });
    }
    keyboards.splice(keyboardIndex, 1);
    res.status(204).json({ message: 'Keyboard deleted successfully' });
});

const PORT = process.env.PORT || 4002;
app.listen(PORT, () => {
    console.log(`Storage Service is running on port ${PORT}`);
});